import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddUsersComponent } from './add-users/add-users.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { LoginPageComponent } from './login-page/login-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AddUsersComponent, EditUsersComponent, ViewUsersComponent, ListUsersComponent, LoginPageComponent]
})
export class AdminPanelModule { }
