import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotAvailableComponent } from './page-not-available/page-not-available.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HeaderComponent, FooterComponent, PageNotAvailableComponent],
  exports: [HeaderComponent, FooterComponent]
})
export class UiModule { }
