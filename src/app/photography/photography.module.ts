import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PackagesComponent } from './packages/packages.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PortfolioComponent, AboutMeComponent, ContactUsComponent, PackagesComponent, TermsAndConditionsComponent]
})
export class PhotographyModule { }
