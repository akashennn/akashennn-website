import { Component, OnInit } from '@angular/core';
import { Blog } from '../classes/blog';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-add-blog-post',
  templateUrl: './add-blog-post.component.html',
  styleUrls: ['./add-blog-post.component.css']
})
export class AddBlogPostComponent implements OnInit {

  blog: Blog;
  blogRef: AngularFirestoreCollection<Blog>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.blog = new Blog;
    this.blogRef = this.afs.collection<Blog>('blog-posts');
  }

  ngOnInit() {
  }

  saveBlogPost(data) {
    console.log(data);
    data.author = 'Akash Perera';
    data.imageUrl = ''
    this.blogRef.add({
      title: data.title, 
      content: data.content,
      author: data.author,
      category: data.category,
      short_description: data.short_description,
      imageUrl: data.imageUrl
    });
  }

}
