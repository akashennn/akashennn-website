export class Blog {

    constructor() { }

    title: string;
    content: string;
    author: string;
    category: string;
    short_description: string;
    imageUrl: string;
    
}