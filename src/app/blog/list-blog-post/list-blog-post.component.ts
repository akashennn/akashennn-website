import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Blog } from '../classes/blog';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-blog-post',
  templateUrl: './list-blog-post.component.html',
  styleUrls: ['./list-blog-post.component.css']
})
export class ListBlogPostComponent implements OnInit {

  private blogCollection: AngularFirestoreCollection<Blog>;
  blog: Observable<Blog[]>;
  
  constructor(
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.blogCollection = afs.collection<Blog>('blog-posts');
    this.blog = this.blogCollection.snapshotChanges().map(actions=> {
      return actions.map(action => {
        const data = action.payload.doc.data() as Blog;
        const id = action.payload.doc.id;
        return {id, ...data};
      })
    })
   }

  ngOnInit() {
  }

  viewBlogPost(id){
    this.router.navigateByUrl('blog-post/view/' + id)
  }

}
