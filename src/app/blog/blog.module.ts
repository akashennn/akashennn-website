import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBlogPostComponent } from './add-blog-post/add-blog-post.component';
import { EditBlogPostComponent } from './edit-blog-post/edit-blog-post.component';
import { ViewBlogPostComponent } from './view-blog-post/view-blog-post.component';
import { ListBlogPostComponent } from './list-blog-post/list-blog-post.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [AddBlogPostComponent, EditBlogPostComponent, ViewBlogPostComponent, ListBlogPostComponent]
})
export class BlogModule { }
