import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Blog } from '../classes/blog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-blog-post',
  templateUrl: './view-blog-post.component.html',
  styleUrls: ['./view-blog-post.component.css']
})
export class ViewBlogPostComponent implements OnInit {

  private blogDoc: AngularFirestoreDocument<Blog>;
  blog: Observable<Blog>;
  private id;
  
  constructor(
    private afs: AngularFirestore,
    private route: ActivatedRoute) {
   }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.blogDoc = this.afs.doc<Blog>('blog-posts/' + id);
    this.blog = this.blogDoc.valueChanges();
  }

}
