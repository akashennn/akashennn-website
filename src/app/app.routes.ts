import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ListBlogPostComponent } from './blog/list-blog-post/list-blog-post.component';
import { PageNotAvailableComponent } from './ui/page-not-available/page-not-available.component';
import { LoginPageComponent } from './admin-panel/login-page/login-page.component';
import { AddBlogPostComponent } from './blog/add-blog-post/add-blog-post.component';
import { ViewBlogPostComponent } from './blog/view-blog-post/view-blog-post.component';
import { ContactUsComponent } from './photography/contact-us/contact-us.component';
import { PackagesComponent } from './photography/packages/packages.component';
import { AboutMeComponent } from './photography/about-me/about-me.component';
import { PortfolioComponent } from './photography/portfolio/portfolio.component';

const appRoutes: Routes = [
    { path: '', component: PortfolioComponent },
    { path: 'login', component: LoginPageComponent },

    { path: 'photography/contact', component: ContactUsComponent },
    { path: 'photography/packages', component: PackagesComponent },
    { path: 'photography/about-me', component: AboutMeComponent },
    { path: 'photography/portfolio', component: PortfolioComponent },

    { path: 'blog-post/new', component: AddBlogPostComponent },
    { path: 'blog-post/view/:id', component: ViewBlogPostComponent },

    { path: '**', component: PageNotAvailableComponent }
];

export const Routing = RouterModule.forRoot(appRoutes, { useHash: false });